const synaptic = require ( 'synaptic' );

const Neutron   = synaptic.Neutron;
const Layer     = synaptic.Layer;
const Network   = synaptic.Network;
const Trainer   = synaptic.Trainer;
const Architect = synaptic.Architect;

const Perception = function ( numberOfInputNodes, numberOfHiddenNodes, numberOfOutputNodes ) {
  
  this.trainer = new Trainer ( this );

  let inputLayer  = new Layer ( numberOfInputNodes  );
  let hiddenLayer = new Layer ( numberOfHiddenNodes );
  let outputLayer = new Layer ( numberOfOutputNodes );

  inputLayer.project ( hiddenLayer );
  hiddenLayer.project ( outputLayer );

  this.set ( {
      input  :   inputLayer
    , hidden : [ hiddenLayer ]
    , output :   outputLayer
  } );

};

Perception.prototype = new Network ();
Perception.prototype.contructor = Perception;

Perception.prototype.train = function ( trainingset ) {

  let trainingOptions = {
      rate       : 0.1
    , iterations : 100000
    , shuffle    : true
    , error      : 0.05
    , log        : 1000
    , cost       : Trainer.cost.CROSS_ENTROPY
    // , cost: Trainer.cost.MSE
  };

  let trainResult = this.trainer.train ( trainingset.toArray (), trainingOptions );
  console.log ( trainResult );
};

module.exports = Perception;

