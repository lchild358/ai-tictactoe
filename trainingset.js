const fs = require ( 'fs' );

const TrainingSet = function () {
  this.data = {};
  this.filePath = null;
};

TrainingSet.prototype.toFile = function ( filePath ) {
  if ( arguments.length != 1 && this.filePath == null ) {
    throw 'TrainingSet.toFile: filePath not specified';
  }

  this.filePath = filePath;

  fs.writeFileSync ( filePath, JSON.stringify ( this.data ) );
  this.filePath = filePath;

  return true;
};

TrainingSet.prototype.fromFile = function ( filePath ) {
  if ( arguments.length != 1 && this.filePath == null ) {
    throw 'TrainingSet.fromFile: filePath not specified';
  }

  this.filePath = filePath;

  if ( ! fs.existsSync ( this.filePath ) ) {
    return false;
  }

  let json = JSON.parse ( fs.readFileSync ( this.filePath ) );
  if ( typeof ( json ) != 'object' || json == null ) {
    throw 'TrainingSet.fromFile: Invalid json data';
  }

  this.data = json;

  return true;
};

TrainingSet.prototype.toArray = function () {
  let rs = [];
  for ( var key in this.data ) {
    rs.push ( this.data [ key ] );
  }
  return rs;
};

TrainingSet.prototype.print = function () {
  for ( index in this.data ) {
    let item = this.data[ index ];
    console.log ( `[ ${ item.input[0] } | ${ item.input[1] } | ${ item.input[2] } ]    [ ${ item.output[0] } | ${ item.output[1] } | ${ item.output[2] } ]` );
    console.log ( `[ ${ item.input[3] } | ${ item.input[4] } | ${ item.input[5] } ] => [ ${ item.output[3] } | ${ item.output[4] } | ${ item.output[5] } ]` );
    console.log ( `[ ${ item.input[6] } | ${ item.input[7] } | ${ item.input[8] } ]    [ ${ item.output[6] } | ${ item.output[7] } | ${ item.output[8] } ]` );
    console.log ( '' );
  };
};

TrainingSet.prototype.add = function ( input, output ) {
  let hflip = function ( input ) {
    return [ 6, 7, 8, 3, 4, 5, 0, 1, 2 ].map ( key => input[ key ] );
  };

  let vflip = function ( input ) {
    return [ 2, 1, 0, 5, 4, 3, 8, 7, 6 ].map ( key => input[ key ] );
  };

  let rotate = function ( input ) {
    return [ 6, 3, 0, 7, 4, 1, 8, 5, 2 ].map ( key => input[ key ] );
  };

  let reverse = function ( input ) {
    return input.map ( val => val == 1 ? 2 : ( val =- 2 ? 1 : 0 ) );
  };

  var input1 = input;
  var input2 = hflip ( input1 );
  var input3 = vflip ( input1 );
  var input4 = vflip ( input2 );
  var input5 = rotate ( input1 );
  var input6 = hflip ( input5 );
  var input7 = vflip ( input5 );
  var input8 = vflip ( input6 );
  var input1r = reverse ( input1 );
  var input2r = reverse ( input2 );
  var input3r = reverse ( input3 );
  var input4r = reverse ( input4 );
  var input5r = reverse ( input5 );
  var input6r = reverse ( input6 );
  var input7r = reverse ( input7 );
  var input8r = reverse ( input8 );

  var output1 = output;
  var output2 = hflip ( output1 );
  var output3 = vflip ( output1 );
  var output4 = vflip ( output2 );
  var output5 = rotate ( output1 );
  var output6 = hflip ( output5 );
  var output7 = vflip ( output5 );
  var output8 = vflip ( output6 );

  var processList = [
      { input: input1, output: output1 }
    , { input: input2, output: output2 }
    , { input: input3, output: output3 }
    , { input: input4, output: output4 }
    , { input: input5, output: output5 }
    , { input: input6, output: output6 }
    , { input: input7, output: output7 }
    , { input: input8, output: output8 }

    , { input: input1r, output: output1 }
    , { input: input2r, output: output2 }
    , { input: input3r, output: output3 }
    , { input: input4r, output: output4 }
    , { input: input5r, output: output5 }
    , { input: input6r, output: output6 }
    , { input: input7r, output: output7 }
    , { input: input8r, output: output8 }
  ];

  for ( var processItem of processList ) {
    let input = processItem.input;
    let output = processItem.output;
  
    let key = JSON.stringify ( input );
    if ( ! this.data [ key ] ) {
      // New entry
      this.data [ key ] = { input: input, output: output };
      // console.log ( 'New entry' );
      // console.log ( `${ input } => ${ output }` );
    } else {
      // Exist entry -> Merge output
      let oldOutput = this.data [ key ].output;
      let newOutput = [];
      for ( let [ index, value ] of oldOutput.entries () ) {
        newOutput [ index ] = ( value == 0 ) ? output [ index ] : 1;
      }
      this.data [ key ].output = newOutput;
      // console.log ( 'Merge entry' );
      // console.log ( `${ this.data [ key ].input } => ${ newOutput }` );
    }
  }
};

module.exports = TrainingSet;
