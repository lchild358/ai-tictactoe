
const TictactoeBoard = function( boardData, nextValue ){

  this.boardData = boardData;
  this.nextValue = nextValue;

};

TictactoeBoard.newBoard = function () {
  return new TictactoeBoard ( [ 0, 0, 0, 0, 0, 0, 0, 0, 0 ], 1 );
};

TictactoeBoard.prototype.print = function () {
  let mappingBoardData = this.boardData.map ( function( val ) {
    return val == 1 ? 'X' : ( val == 2 ? 'O' : ' ');
  });

  console.log( `[ ${ mappingBoardData[ 0 ] } | ${ mappingBoardData[ 1 ] } | ${ mappingBoardData[ 2 ] } ]` );
  console.log( `[ ${ mappingBoardData[ 3 ] } | ${ mappingBoardData[ 4 ] } | ${ mappingBoardData[ 5 ] } ]` );
  console.log( `[ ${ mappingBoardData[ 6 ] } | ${ mappingBoardData[ 7 ] } | ${ mappingBoardData[ 8 ] } ]` );
  console.log( '' );
};

TictactoeBoard.prototype.checkForWinner = function () {
  let positionsList = [
    [ 0, 1, 2 ], [ 3, 4, 5 ], [ 6, 7, 8 ],
    [ 0, 3, 6 ], [ 1, 4, 7 ], [ 2, 5, 8 ],
    [ 0, 4, 8 ], [ 2, 4, 6 ]
  ];

  for ( var positions of positionsList ) {
    let filteredPositions = positions.filter ( key => this.boardData[ key ] != 0 );
    if ( filteredPositions.length != 3 ) continue;

    let sum = 0;
    filteredPositions.forEach ( key => sum += this.boardData[ key ]);

    if ( sum == 3 ) return 1;
    if ( sum == 6 ) return 2;
  };

  return 0;
};

TictactoeBoard.prototype.set = function ( additionDataMap ) {
  return new TictactoeBoard (
      Array.from ( this.boardData.keys () ).map ( key => this.boardData[ key ] == 0 && additionDataMap[ key ] == 1 ? this.nextValue : this.boardData[ key ] )
    , this.nextValue == 1 ? 2 : 1
  );
};

TictactoeBoard.prototype.getNext = function ( perception ) {

  let result = perception.activate ( this.boardData );
  
  let resultKeys = Array.from ( result.keys() ).filter ( key => Math.round ( result[key] ) == 1 && this.boardData[ key ] == 0 );
  if ( resultKeys.length <= 0 ){
    /* */ console.log( '>> rand' );
    resultKeys = Array.from( this.boardData.keys() ).filter( key => this.boardData[key] == 0 );
    if ( resultKeys.length <= 0 ){
      return null;
    }
  } else {
    /* */ console.log( '>> ai ' );
  }

  let randomKey = resultKeys[ Math.floor( Math.random() * resultKeys.length ) ];
  return [ 0, 1, 2, 3, 4, 5, 6, 7, 8 ].map( val => val == randomKey ? 1 : 0 );
};

module.exports = TictactoeBoard;
