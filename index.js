const TrainingSet = require ( './trainingset.js' );
const Perception = require ( './perception.js' );
const TictactoeBoard = require ( './tictactoeboard.js' );
const Histories = require ( './histories.js' );

let perception  = new Perception ( 9, 18, 9 );
let trainingset = new TrainingSet ();

if ( trainingset.fromFile ( './index.txt' ) ) {
  console.log ( 'Training...' );
  perception.train ( trainingset );
}

let tictactoeBoard = TictactoeBoard.newBoard ();
let histories = new Histories ();

let winFlg = false;

for(var i = 0; i < 9; i++){
  let nextData = tictactoeBoard.getNext ( perception );
  histories.add ( tictactoeBoard, nextData );

  tictactoeBoard = tictactoeBoard.set ( nextData );
  let result = tictactoeBoard.checkForWinner ();

  if ( result != 0 ) {
    histories.add ( tictactoeBoard, null );
    histories.print ();

    if ( result == 1 ) {
      console.log ( 'X won!' );
    } else if (result == 2 ) {
      console.log ( 'O won!' );
    }

    for ( var index = histories.length() - 2; index >= 0; index -= 2 ) {
      let history = histories.getAt ( index );
      let input   = history.board.boardData;

      if ( result == 2 ) {
        input = input.map ( val => val == 1 ? 2 : ( val == 2 ? 1 : 0 ) );
      }
      let output  = history.nextData;

      // console.log ();
      // console.log ( 'Add training set' );
      // console.log ( `${ input } => ${ output }` );
      trainingset.add ( input, output );
      trainingset.toFile ( './index.txt' );

      // console.log ();
      // trainingset.print ();
      break;
    }

    winFlg = true;
    break;
  }
}

if ( ! winFlg ) console.log( 'Draw!' );
