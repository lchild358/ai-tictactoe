const Histories = function () {
  this.data = [];
};

Histories.prototype.add = function ( tictactoeBoard, nextData ) {
  this.data.push ( {
      board: tictactoeBoard
    , nextData: nextData
  } );
};

Histories.prototype.print = function () {
  for ( var [ index, history ] of this.data.entries () ) {
    console.log ( `Step ${ index + 1 }.` );
    history.board.print ();
  }
};

Histories.prototype.length = function () {
  return this.data.length;
};

Histories.prototype.getAt = function ( index ) {
  return index < 0 || index >= this.data.length ? null : this.data[ index ];
};

module.exports = Histories;

